package com.example.http;

import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.impl.VertxThread;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.RunTestOnContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author zsh
 * 21/Feb/2020
 */
@RunWith(VertxUnitRunner.class)
public class MainRestAPITest {
    private static final Logger LOG = LoggerFactory.getLogger(MainRestAPITest.class);
    @Rule
    public RunTestOnContext rule = new RunTestOnContext();

    @Before
    public void setup(TestContext context) {
//        Vertx vertx = rule.vertx();
        Async async = context.async();
        LOG.info("THREAD: {}", Thread.currentThread().getName());
        // read file test/resources/config.json
        ConfigStoreOptions localJsonStore = new ConfigStoreOptions()
                .setType("file")
                .setConfig(new JsonObject().put("path", "config.json"));

        ConfigRetriever retriever = ConfigRetriever.create(rule.vertx(),
                new ConfigRetrieverOptions().addStore(localJsonStore));

        retriever.getConfig(ar -> {
            if (ar.failed()) {
                LOG.info("read config failed");
                return;
            }
            DeploymentOptions options = new DeploymentOptions().setConfig(ar.result());
            rule.vertx().deployVerticle(new MainRestAPI(), options, dar -> {
                context.assertTrue(dar.succeeded());
                LOG.info("MainRestAPI deployed OK");
                async.complete();
            });
        });

    }

    // you should see the configurations in test/resources/config.json
    @Test
    public void test(TestContext context) {
        Thread thread = Thread.currentThread();
        LOG.info("THREAD: {}", thread.getName());
        if (thread instanceof VertxThread) {
           VertxThread vertxThread = (VertxThread) thread;
           LOG.info("THREAD_GROUP: {}", vertxThread.getThreadGroup().getName());
        }

    }
}
