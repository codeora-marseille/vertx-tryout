package com.example.context;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Context;
import io.vertx.core.Vertx;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author szhao 2/11/19
 */
public class ContextSameEventLoop {
    private static final Logger logger = LoggerFactory.getLogger(ContextSameEventLoop.class);
    public static void main(String[] args) {

        Vertx vertx = Vertx.vertx();
        System.out.println("Current context is " + Vertx.currentContext());
        final Context context = vertx.getOrCreateContext();
        context.runOnContext(v -> {
            logger.info("ABC");
        });
        context.runOnContext(v -> {
            logger.info("123");
        });
        vertx.deployVerticle(new AbstractVerticle() {
            @Override
            public void start() throws Exception {
                System.out.println("Current context is " + Vertx.currentContext());
                System.out.println("Verticle context is " + context);
            }
        });
    }
}
/*
This main function is not in a Verticle and there is no associated context, so we have context as null

But as a new verticle is deployed, inside the verticle a context is implicitly created by the framework for us

> Task :ContextSameEventLoop.main()
Current context is null
253 [vert.x-eventloop-thread-0] INFO com.example.context.ContextSameEventLoop - ABC
253 [vert.x-eventloop-thread-0] INFO com.example.context.ContextSameEventLoop - 123
Current context is io.vertx.core.impl.EventLoopContext@6ebbbcda
Verticle context is io.vertx.core.impl.EventLoopContext@6ebbbcda
 */