package com.example.context;

import io.vertx.core.Vertx;

/**
 * @author zsh
 * 17/Jul/2020
 */
public class VertxDeployContext2 {
    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();
        System.out.println("M1 - Current context is " + Vertx.currentContext());
        vertx.deployVerticle(new TrivialVerticle(), ar ->{
            System.out.println("M2 - Current context is " + Vertx.currentContext());
        });
    }
}
/*
> Task :VertxDeployContext2.main()
M1 - Current context is null
T1 - Current context is io.vertx.core.impl.EventLoopContext@43425a0a
T2 - Verticle context is io.vertx.core.impl.EventLoopContext@43425a0a
M2 - Current context is io.vertx.core.impl.EventLoopContext@72ff4dc4
 */
