package com.example.context;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;

/**
 * A verticle object is essentially the combination of 2 objects:
 *
 * - the Vert.x instance it belongs to (the vertx instance in Verticle), and
 * - a dedicated context instance that allows dispatching events to handlers.
 *
 * @author zsh
 * 17/Jul/2020
 */
public class TrivialVerticle extends AbstractVerticle {
    @Override
    public void start(Promise<Void> startPromise) throws Exception {
        System.out.println("T1 - Current context is " + Vertx.currentContext());
        System.out.println("T2 - Verticle context is " + context);
        startPromise.complete();
    }
}
