package com.example.context;

import io.vertx.core.Vertx;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * -  calling getOrCreateContext() from a context thread like the one of a verticle returns the context, while
 * - calling getOrCreateContext() from a non-context thread creates a new context.
 * @author szhao 2/11/19
 */
public class ContextOnDifferentEventLoops {
    private static final Logger logger = LoggerFactory.getLogger(ContextOnDifferentEventLoops.class);
    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();

        vertx.getOrCreateContext().runOnContext(v -> {logger.info("ABC");});
        vertx.getOrCreateContext().runOnContext(v -> {logger.info("123");});

    }
}
