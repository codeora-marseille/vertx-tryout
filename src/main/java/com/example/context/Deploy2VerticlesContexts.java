package com.example.context;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;

/**
 * @author zsh
 * 17/Jul/2020
 */
public class Deploy2VerticlesContexts {
    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();
        System.out.println("main - Current context is " + Vertx.currentContext());

        vertx.deployVerticle(new AbstractVerticle() {
            public void start() throws Exception {
                System.out.println("vert A - Current context is " + Vertx.currentContext());
                System.out.println("vert A - Verticle context is " + context);
//                System.exit(0);
            }
        });

        System.out.println("vertx L0 = " + vertx);
        vertx.deployVerticle(new AbstractVerticle() {
            @Override
            public void start() throws Exception {
                System.out.println("vert B - Current context is " + Vertx.currentContext());
                System.out.println("vert B - Verticle context is " + context);
                // the vertx here is inherited from AbstractVerticle
                System.out.println("vertx L1 = " + vertx);
                vertx.deployVerticle(new AbstractVerticle() {
                    @Override
                    public void start() throws Exception {
                        System.out.println("vertx L2 = " + vertx);
                        System.out.println("Vert C - Current context is " + Vertx.currentContext());
                        System.out.println("Vert C - Verticle context is " + context);
                    }
                });
            }
        });
    }
}

/*
for all the verticles, they have the same vertx instance, but each of them has different context

main - Current context is null
vertx L0 = io.vertx.core.impl.VertxImpl@5f341870
vert A - Current context is io.vertx.core.impl.EventLoopContext@4117b264
vert B - Current context is io.vertx.core.impl.EventLoopContext@758ff31d
vert B - Verticle context is io.vertx.core.impl.EventLoopContext@758ff31d
vertx L1 = io.vertx.core.impl.VertxImpl@5f341870
vert A - Verticle context is io.vertx.core.impl.EventLoopContext@4117b264
vertx L2 = io.vertx.core.impl.VertxImpl@5f341870
Vert C - Current context is io.vertx.core.impl.EventLoopContext@72ff4dc4
Vert C - Verticle context is io.vertx.core.impl.EventLoopContext@72ff4dc4
 */