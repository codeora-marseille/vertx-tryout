package com.example.context;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;

/**
 * @author zsh
 * 17/Jul/2020
 */
public class VertxDeployContext {
    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();
        System.out.println("A - Current context is " + Vertx.currentContext());

        vertx.deployVerticle(new AbstractVerticle() {
            public void start() throws Exception {
                System.out.println("B - Current context is " + Vertx.currentContext());
                System.out.println("C - Verticle context is " + context);
//                System.exit(0);
            }
        });

        System.out.println("D - Current context is " + Vertx.currentContext());
    }
}

/*
Verticle has a context
> Task :VertxDeployContext.main()
A - Current context is null
D - Current context is null
B - Current context is io.vertx.core.impl.EventLoopContext@7590a5ae
C - Verticle context is io.vertx.core.impl.EventLoopContext@7590a5ae
 */
