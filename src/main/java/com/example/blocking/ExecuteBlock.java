package com.example.blocking;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author szhao 2/11/19
 * It may not always be a good idea to use WorkerVerticle, which could lead to an explosion of worker threads
 * A better solutions is to use a worker thread pool: handle the blocking task to a worker thread and set a handler
 * for the asynchronous result
 *
 * Execute this class and you will see handle off from event loop to worker thread
 */
public class ExecuteBlock extends AbstractVerticle {
    private static final Logger logger = LoggerFactory.getLogger(ExecuteBlock.class);
    @Override
    public void start() {
        vertx.setPeriodic(5_000, id -> {
            logger.info("Tick");
            vertx.executeBlocking(this::blockingCodeHandler, this::resultHandler);
        });
    }

    private void blockingCodeHandler(Promise<String> promise) {
        logger.info("Blocking code running");
        try {
            Thread.sleep(4_000);
            logger.info("Done");
            promise.complete("OK");
        } catch (InterruptedException e) {
            logger.error("Interrupted", e);
            promise.fail(e);
        }
    }

    private void resultHandler(AsyncResult<String> result) {
        if (result.succeeded()) {
            logger.info("result: {}", result.result());
        }
        else {
            logger.error("Whoops {}", result.cause());
        }
    }

    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(new ExecuteBlock());
    }

}
