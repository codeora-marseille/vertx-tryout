package com.example.blocking;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author szhao 2/11/19
 *
 * when blocking code is inevitable, we use worker verticle,
 * which executes on a worker thread instead of event-loop
 */
public class WorkerVerticle extends AbstractVerticle {
    private static final Logger logger = LoggerFactory.getLogger(WorkerVerticle.class);
    @Override
    public void start() {
        vertx.setPeriodic(10_000, this::periodicTask);
    }

    private void periodicTask(Long timerId) {
        try {
            logger.info("Zzzz...");
            Thread.sleep(8_000);
            logger.info("Wake up!");
        } catch (InterruptedException ex) {
            logger.error("Interrupted", ex);
        }
    }

    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();
        // by setting worker to true, we tell vertx this is going to be executed on a worker thread
        DeploymentOptions options = new DeploymentOptions().setInstances(2).setWorker(true);
        vertx.deployVerticle(WorkerVerticle.class.getName(), options);
    }

}
