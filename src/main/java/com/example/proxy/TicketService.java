package com.example.proxy;

import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;

/**
 * @author zsh
 * 18/Feb/2020
 */
@ProxyGen
public interface TicketService {
    static final String ADDR = "service.ticket";

    static TicketService create(Vertx vertx) {
        return new TicketServiceImpl(vertx);
    }

    static TicketService createProxy(Vertx vertx) {
        // EBProxy means Event Bus Proxy
        return new TicketServiceVertxEBProxy(vertx, ADDR);
    }

    void uploadAttachment(String fleetId, Handler<AsyncResult<JsonObject>> handler);
}
