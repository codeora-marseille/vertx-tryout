package com.example.proxy;

import io.vertx.core.AbstractVerticle;
import io.vertx.serviceproxy.ServiceBinder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author zsh
 * 18/Feb/2020
 */
public class TicketVerticle extends AbstractVerticle {
    private static final Logger LOG = LoggerFactory.getLogger(TicketVerticle.class);
    @Override
    public void start() {
       LOG.info("registering services");
       TicketService ticketService = TicketService.create(vertx);
       // the binding can be done anywhere, this verticle itself is not necessary
       new ServiceBinder(vertx).setAddress(TicketService.ADDR).register(TicketService.class, ticketService);
       LOG.info("ticket service registered at {}", TicketService.ADDR);
    }
}
