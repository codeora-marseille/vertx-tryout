package com.example.proxy;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;

/**
 * @author zsh
 * 18/Feb/2020
 */
public class TicketServiceImpl implements TicketService {
    private static final Logger LOG = LoggerFactory.getLogger(TicketServiceImpl.class);

    private final Vertx vertx;

    public TicketServiceImpl(Vertx vertx) {
       this.vertx = vertx;
    }

    @Override
    public void uploadAttachment(String fleetId, Handler<AsyncResult<JsonObject>> handler) {
        LOG.info("upload new attachment, {}", vertx.getOrCreateContext());
        Promise<JsonObject> promise = Promise.promise();
        int sum = new Random().nextInt();
        for (int i = 0; i < 100; i++) {
            // just do something
            sum = (sum + i)  % 37;
        }
        promise.complete(new JsonObject().put("result", sum));
        handler.handle(promise.future());
    }
}
