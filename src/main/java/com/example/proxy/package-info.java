/**
 * @author zsh
 * 18/Feb/2020
 */

@ModuleGen(name = "proxy", groupPackage = "com.example")
package com.example.proxy;

import io.vertx.codegen.annotations.ModuleGen;