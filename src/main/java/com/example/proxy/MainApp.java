package com.example.proxy;

import io.vertx.core.Vertx;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author zsh
 * 18/Feb/2020
 */
public class MainApp {
    private static final Logger LOG = LoggerFactory.getLogger(MainApp.class);

    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(TicketVerticle.class.getName());

        TicketService proxy = TicketService.createProxy(vertx);
        for (int i = 0; i < 10; i++) {

            // we call the method directly, like synchronous calls, but underneath implemented with event bus
            proxy.uploadAttachment("FOOO1", ar -> {
                LOG.info("Get response from ticket service: {}", ar.result().encode());
            });
        }
    }
}
