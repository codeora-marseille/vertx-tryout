package com.example.thread;

import io.vertx.core.Handler;
import io.vertx.core.Vertx;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zsh
 * 17/Jul/2020
 */
public class App2 {
    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();
        List<Handler<Long>> handlers = new ArrayList<>();
        for (int i = 0; i < 5; i++) handlers.add(id -> System.out.println(id + " : " + Thread.currentThread()));
        for (int i = 1; i < 30; i++) {
            // callbacks will be called on different threads!
            vertx.setTimer(i * 1000, handlers.get(i % 5));
        }
    }
}
