package com.example.thread;

import io.vertx.core.Handler;
import io.vertx.core.Vertx;

import java.util.ArrayList;
import java.util.List;

/**
 * An event loop context executes handlers on an event loop: handlers are executed directly on the IO threads, as a consequence:
 *
 * - A handler will always be executed with the same thread
 *
 * - A handler must never block the thread, otherwise it will create starvation for all the IO tasks associated with that event loop.
 *
 * This behavior allows for a greatly simplified threading model by guaranteeing that associated handlers will always be executed on the same thread, thus removing the need for synchronization and other locking mechanisms.
 * @author zsh
 * 17/Jul/2020
 */
public class App {
    public static void main(String[] args) {
        System.out.println(Thread.currentThread());

        Vertx vertx = Vertx.vertx();


        for (int i = 1; i < 30; i++) {
            // callbacks will be called on different threads!
            vertx.setTimer(i * 1000, tid -> {
                System.out.println(tid + ": " + Thread.currentThread());
            });
        }
    }
}
