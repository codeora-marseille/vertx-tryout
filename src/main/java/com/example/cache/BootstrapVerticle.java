package com.example.cache;

import io.vertx.core.AbstractVerticle;

/**
 * @author szhao 2/20/19
 *
 * https://vertx.io/docs/vertx-config/java/#_configuring_a_set_of_verticles
 *
 * Configure a set of verticles or configure vertx itself here
 */
public class BootstrapVerticle extends AbstractVerticle {

}
