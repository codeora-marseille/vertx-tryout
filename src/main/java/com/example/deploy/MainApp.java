package com.example.deploy;

import io.vertx.core.Vertx;

/**
 * @author szhao 2/7/19
 *
 * example of deploying verticles
 */
public class MainApp {
    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(new Deployer());
    }
}
