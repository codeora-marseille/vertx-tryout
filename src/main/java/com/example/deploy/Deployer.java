package com.example.deploy;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Verticle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.stream.IntStream;


/**
 * @author szhao 2/7/19
 *
 * $ ./gradlew run -PmainClass=com.example.deploy.MainApp
 *
 * Note the threads information, verticles could share event loop threads
 *
 */
public class Deployer extends AbstractVerticle {
    private static final Logger logger = LoggerFactory.getLogger(Deployer.class);

    @Override
    public void start() {
        IntStream.range(1, 20).forEach(i -> deploy(i * 1000, new EmptyVerticle()));
    }

    private void deploy(int delay, Verticle v) {
        vertx.setTimer(delay, id -> vertx.deployVerticle(v, ar -> {
            if (ar.succeeded()) {
                logger.info("successfully deployed {}", ar.result());
            }
            else {
                logger.error("error while deploying {}", ar.cause());
            }
        }));
    }

    private void undeploy(String id) {
        vertx.undeploy(id, ar -> {
            if (ar.succeeded()) {
                logger.info("successfully undeployed {}", id);
            }
            else {
                logger.error("error while undeploying {}", id);
            }
        });
    }

}
