package com.example.deploy;

import io.vertx.core.AbstractVerticle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author szhao 2/7/19
 */
public class EmptyVerticle extends AbstractVerticle {
    private static final Logger logger = LoggerFactory.getLogger(AbstractVerticle.class);

    @Override
    public void start() {
        logger.info("start..");
    }

    @Override
    public void stop() {
        logger.info("stop");
    }
}
