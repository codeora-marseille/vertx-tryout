package com.example.loop;

import io.vertx.core.AbstractVerticle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author zsh
 * 17/Jul/2020
 */
public class SingleTimerThread extends AbstractVerticle  {
    public static final Logger log = LoggerFactory.getLogger(SingleTimerThread.class);

    @Override
    public void start() throws Exception {
        System.out.println("SingleTimerThread main " + Thread.currentThread());
//        Vertx vertx = Vertx.vertx();
        for (int i = 1; i < 20; i++) {
            int index = i;
            vertx.setTimer( index * 1000, timerID -> {
                // timer handler will run in SAME threads
                System.out.println("SingleTimerThread " + index + ": " + Thread.currentThread());
            });
        }
    }
}
