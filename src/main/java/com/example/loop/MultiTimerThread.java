package com.example.loop;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * an event loop context guarantees to use the same thread,
 * the same thread could be used by various event loops
 *
 * the number of threads created is by default twice the number of CPU cores.
 *
 * @author zsh
 * 17/Jul/2020
 */
public class MultiTimerThread extends AbstractVerticle {
    public static final Logger log = LoggerFactory.getLogger(MultiTimerThread.class);
    @Override
    public void start(Promise<Void> startFuture) throws Exception {
        System.out.println(Thread.currentThread());
        Vertx vertx = Vertx.vertx();

        System.out.println(vertx);
        for (int i = 1; i < 20; i++) {
            int index = i;
            vertx.setTimer( index * 1000, timerID -> {
                // timer handler will run in DIFFERENT threads
               System.out.println("MutliTimerThread " + index + ": " + Thread.currentThread());
            });
        }
        startFuture.complete();
    }
}
