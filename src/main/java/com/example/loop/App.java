package com.example.loop;

import io.vertx.core.Vertx;

/**
 *  By default Vert.x creates twice the number of event-loop threads as CPU cores.
 *  If you have 8 cores then a Vert.x application has 16 event-loops.
 *  The assignment of verticles to event-loops is done in a round-robin fashion.
 *
 * This teaches us an interesting lesson: while a verticle always uses the same event-loop thread,
 * the event-loop threads are being shared by multiple verticles.
 * This design results in a predictable number of threads for running an application.
 *
 * @author zsh
 * 17/Jul/2020
 */
public class App {
    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();
        System.out.println("Main thread: " + Thread.currentThread());
        vertx.deployVerticle(new MultiTimerThread(), ar ->{
            // This handler will run on a different thread
            System.out.println("deploy handler thread: " + Thread.currentThread());
        });
        // the deployed verticle, if using the inherited vertx from AbstractVerticle or using the passed along vertx instance,
        // will always run on the same thread, and the handlers also

        // however, if new vertx instance in created in side the verticle (which is not recommended), the handlers could run
        // on different threads, like the example MultiTimerThread
        vertx.deployVerticle(new SingleTimerThread());
        vertx.deployVerticle(new InheritedVertxTimerThread(vertx));
    }
}

/*

Notice that the verticle InheritedVertxTimerThread and also SingleTimerThread always run on the same thread
handlers will be executed on the same thread if there is a context

It bad practice for a verticle to create new Vertx instance like MultiTimerThread

deploy handler thread: Thread[vert.x-eventloop-thread-3,5,main]
SingleTimerThread 1: Thread[vert.x-eventloop-thread-1,5,main]
InheritedVertxTimerThread 1: Thread[vert.x-eventloop-thread-2,5,main]
MutliTimerThread 1: Thread[vert.x-eventloop-thread-0,5,main]
InheritedVertxTimerThread 2: Thread[vert.x-eventloop-thread-2,5,main]
SingleTimerThread 2: Thread[vert.x-eventloop-thread-1,5,main]
MutliTimerThread 2: Thread[vert.x-eventloop-thread-1,5,main]
InheritedVertxTimerThread 3: Thread[vert.x-eventloop-thread-2,5,main]
SingleTimerThread 3: Thread[vert.x-eventloop-thread-1,5,main]
MutliTimerThread 3: Thread[vert.x-eventloop-thread-2,5,main]
InheritedVertxTimerThread 4: Thread[vert.x-eventloop-thread-2,5,main]
SingleTimerThread 4: Thread[vert.x-eventloop-thread-1,5,main]
MutliTimerThread 4: Thread[vert.x-eventloop-thread-3,5,main]
SingleTimerThread 5: Thread[vert.x-eventloop-thread-1,5,main]
InheritedVertxTimerThread 5: Thread[vert.x-eventloop-thread-2,5,main]
MutliTimerThread 5: Thread[vert.x-eventloop-thread-4,5,main]
InheritedVertxTimerThread 6: Thread[vert.x-eventloop-thread-2,5,main]
SingleTimerThread 6: Thread[vert.x-eventloop-thread-1,5,main]
MutliTimerThread 6: Thread[vert.x-eventloop-thread-5,5,main]
SingleTimerThread 7: Thread[vert.x-eventloop-thread-1,5,main]
InheritedVertxTimerThread 7: Thread[vert.x-eventloop-thread-2,5,main]
MutliTimerThread 7: Thread[vert.x-eventloop-thread-6,5,main]
InheritedVertxTimerThread 8: Thread[vert.x-eventloop-thread-2,5,main]
SingleTimerThread 8: Thread[vert.x-eventloop-thread-1,5,main]
MutliTimerThread 8: Thread[vert.x-eventloop-thread-7,5,main]
InheritedVertxTimerThread 9: Thread[vert.x-eventloop-thread-2,5,main]
SingleTimerThread 9: Thread[vert.x-eventloop-thread-1,5,main]
MutliTimerThread 9: Thread[vert.x-eventloop-thread-8,5,main]
SingleTimerThread 10: Thread[vert.x-eventloop-thread-1,5,main]
InheritedVertxTimerThread 10: Thread[vert.x-eventloop-thread-2,5,main]
MutliTimerThread 10: Thread[vert.x-eventloop-thread-9,5,main]
InheritedVertxTimerThread 11: Thread[vert.x-eventloop-thread-2,5,main]
SingleTimerThread 11: Thread[vert.x-eventloop-thread-1,5,main]
MutliTimerThread 11: Thread[vert.x-eventloop-thread-10,5,main]
SingleTimerThread 12: Thread[vert.x-eventloop-thread-1,5,main]
InheritedVertxTimerThread 12: Thread[vert.x-eventloop-thread-2,5,main]
MutliTimerThread 12: Thread[vert.x-eventloop-thread-11,5,main]
SingleTimerThread 13: Thread[vert.x-eventloop-thread-1,5,main]
InheritedVertxTimerThread 13: Thread[vert.x-eventloop-thread-2,5,main]
MutliTimerThread 13: Thread[vert.x-eventloop-thread-12,5,main]
SingleTimerThread 14: Thread[vert.x-eventloop-thread-1,5,main]
InheritedVertxTimerThread 14: Thread[vert.x-eventloop-thread-2,5,main]
MutliTimerThread 14: Thread[vert.x-eventloop-thread-13,5,main]
InheritedVertxTimerThread 15: Thread[vert.x-eventloop-thread-2,5,main]
SingleTimerThread 15: Thread[vert.x-eventloop-thread-1,5,main]
MutliTimerThread 15: Thread[vert.x-eventloop-thread-14,5,main]
InheritedVertxTimerThread 16: Thread[vert.x-eventloop-thread-2,5,main]
SingleTimerThread 16: Thread[vert.x-eventloop-thread-1,5,main]
MutliTimerThread 16: Thread[vert.x-eventloop-thread-15,5,main]
SingleTimerThread 17: Thread[vert.x-eventloop-thread-1,5,main]
InheritedVertxTimerThread 17: Thread[vert.x-eventloop-thread-2,5,main]
MutliTimerThread 17: Thread[vert.x-eventloop-thread-0,5,main]
InheritedVertxTimerThread 18: Thread[vert.x-eventloop-thread-2,5,main]
SingleTimerThread 18: Thread[vert.x-eventloop-thread-1,5,main]
MutliTimerThread 18: Thread[vert.x-eventloop-thread-1,5,main]
SingleTimerThread 19: Thread[vert.x-eventloop-thread-1,5,main]
InheritedVertxTimerThread 19: Thread[vert.x-eventloop-thread-2,5,main]
MutliTimerThread 19: Thread[vert.x-eventloop-thread-2,5,main]

Process finished with exit code 130 (interrupted by signal 2: SIGINT)

 */