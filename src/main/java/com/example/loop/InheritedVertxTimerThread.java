package com.example.loop;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;

/**
 * @author zsh
 * 17/Jul/2020
 */
public class InheritedVertxTimerThread extends AbstractVerticle {

    private Vertx vertx;
    public InheritedVertxTimerThread(Vertx vertx) {
        this.vertx = vertx;
    }

    @Override
    public void start() throws Exception {
        for (int i = 1; i < 20; i++) {
            int index = i;
            this.vertx.setTimer( index * 1000, timerID -> {
                // timer handler will run in SAME threads
                System.out.println("InheritedVertxTimerThread " +  index + ": " + Thread.currentThread());
            });
        }
    }
}
