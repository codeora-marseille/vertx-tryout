package com.example.event;

/**
 * @author szhao 2/13/19
 */
public class SensorEvent {
    private String id;
    private double temp;

    public SensorEvent(String id, double temp) {
        this.id = id;
        this.temp = temp;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getTemp() {
        return temp;
    }

    public void setTemp(double temp) {
        this.temp = temp;
    }
}
