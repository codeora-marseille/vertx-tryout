package com.example.event;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.EventBus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DecimalFormat;

/**
 * @author szhao 2/15/19
 */
public class TemperatureUpdateListener extends AbstractVerticle {
    private static final Logger logger = LoggerFactory.getLogger(TemperatureUpdateListener.class);
    private static final DecimalFormat format = new DecimalFormat("#.##");
    @Override
    public void start() {
        EventBus bus = vertx.eventBus();
        bus.<SensorEvent>consumer("sensor.updates", msg -> {
            SensorEvent event = msg.body();
            logger.info("{} reports temperature {}", event.getId(), event.getTemp());
        });
    }
}
