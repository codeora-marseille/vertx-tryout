package com.example.event;

import io.vertx.core.AbstractVerticle;

import java.util.Random;
import java.util.UUID;

/**
 * @author szhao 2/12/19
 */
public class TemperatureSensorEmulator extends AbstractVerticle {
    private Random random = new Random();
    private String id = UUID.randomUUID().toString();


    @Override
    public void start() {
        scheduleNextUpdate();
    }

    private void scheduleNextUpdate() {
        vertx.setTimer(random.nextInt(5000) + 1000, this::update);
    }

    private void update(Long timerId) {

        SensorEvent event = new SensorEvent(this.id, delta());
        vertx.eventBus().publish("sensor.updates", event);
        scheduleNextUpdate();
    }

    private double delta() {
        if (random.nextInt() > 0) {
            return random.nextGaussian();
        }
        else {
            return -random.nextGaussian();
        }
    }
}
