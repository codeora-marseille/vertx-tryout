package com.example.event;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.MessageCodec;
import io.vertx.core.json.JsonObject;

/**
 * @author szhao 2/15/19
 */
public class SensorEventCodec implements MessageCodec<SensorEvent, SensorEvent> {
    @Override
    public void encodeToWire(Buffer buffer, SensorEvent sensorEvent) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.put("id", sensorEvent.getId());
        jsonObject.put("temp", sensorEvent.getTemp());

        String jsonString = jsonObject.encode();
        int length = jsonString.getBytes().length;

        buffer.appendInt(length);
        buffer.appendString(jsonString);
    }

    @Override
    public SensorEvent decodeFromWire(int pos, Buffer buffer) {
        int length = buffer.getInt(pos);

        String jsonString = buffer.getString(pos + 4, pos + length);
        JsonObject contentJson = new JsonObject(jsonString);

        String id = contentJson.getString("id");
        double temp = contentJson.getDouble("temp");

        return new SensorEvent(id, temp);
    }

    @Override
    public SensorEvent transform(SensorEvent sensorEvent) {
        return sensorEvent;
    }

    @Override
    public String name() {
        return this.getClass().getName();
    }

    @Override
    public byte systemCodecID() {
        return -1;
    }
}
