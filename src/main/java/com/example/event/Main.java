package com.example.event;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;

/**
 * @author szhao 2/15/19
 */
public class Main {
    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();
        vertx.eventBus().registerDefaultCodec(SensorEvent.class, new SensorEventCodec());
        vertx.deployVerticle(TemperatureSensorEmulator.class.getName(), new DeploymentOptions().setInstances(4));
        vertx.deployVerticle(TemperatureUpdateListener.class.getName());
    }
}
