package com.example.http;

import io.vertx.core.Vertx;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author zsh
 * 20/Feb/2020
 */
public class MainApp {
    private static final Logger LOG = LoggerFactory.getLogger(MainApp.class);
    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(new MainRestAPI(), ar -> {
            LOG.info("deploy main verticle OK");
        });
    }
}
