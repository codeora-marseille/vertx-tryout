package com.example.http;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Verticle;
import io.vertx.ext.web.Router;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author zsh
 * 20/Feb/2020
 */
public class MainRestAPI extends AbstractVerticle {
    private static final Logger LOG = LoggerFactory.getLogger(MainRestAPI.class);
    @Override
    public void start(Future<Void> startFuture) {
        Router router = Router.router(vertx);
        LOG.info("===== MAIN CONF ===== \n " + config().encode());

        router.get("/home").handler(ctx -> {
           ctx.response().end("Welcome!");
        });
        Router subRouter = Router.router(vertx);
        router.mountSubRouter("/tickets", subRouter);

        Verticle sub = new SubRestAPI(subRouter);
        vertx.deployVerticle(sub, ar -> {
            LOG.info("deploy sub rest api OK");
            vertx.createHttpServer().requestHandler(router::accept).listen(8809, "localhost", http ->{
                LOG.info("HTTP server started");
                startFuture.complete();
            });
        });
    }
}
