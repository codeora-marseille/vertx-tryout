package com.example.http;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.ext.web.Router;

/**
 * @author zsh
 * 20/Feb/2020
 */

public class SubRestAPI extends AbstractVerticle {
    private final Router router;

    public SubRestAPI(Router router) {
        super();
        this.router = router;
    }

    @Override
    public void start(Future<Void> startFuture) {
        router.get("/:ticketId").handler(c -> {
            String ticketId = c.request().getParam("ticketId");
            c.response().end("ticket-" + ticketId);
        });
        startFuture.complete();
    }
}
