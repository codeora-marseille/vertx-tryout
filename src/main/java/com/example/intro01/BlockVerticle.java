package com.example.intro01;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BlockVerticle extends AbstractVerticle {
    private static final Logger logger = LoggerFactory.getLogger(BlockVerticle.class);
    @Override
    public void start() throws Exception {
        logger.info("sleeping .... ");
        Thread.sleep(10000);
    }

    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(new BlockVerticle());
    }
}
