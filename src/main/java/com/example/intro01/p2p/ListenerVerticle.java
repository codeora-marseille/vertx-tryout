package com.example.intro01.p2p;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

public class ListenerVerticle extends AbstractVerticle {
    public static final Logger logger = LoggerFactory.getLogger(ListenerVerticle.class);
    public final UUID id = UUID.randomUUID();
    @Override
    public void start() throws Exception {
        vertx.eventBus().<JsonObject>consumer("some.address.here", msg -> {
            logger.info("{} received message {}", id, msg.body());
        });
    }
}
