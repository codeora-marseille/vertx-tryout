package com.example.intro01.p2p;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;

public class Main {
    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(ListenerVerticle.class, new DeploymentOptions().setInstances(3));
        vertx.deployVerticle(new PublisherVerticle());
    }
}
