package com.example.intro01.p2p;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonObject;

public class PublisherVerticle extends AbstractVerticle {
    @Override
    public void start() {
        vertx.setPeriodic(1000, timerId -> {
            vertx.eventBus().send("some.address.here",
                    new JsonObject().put("temperature", 25));
        });
    }
}
