package com.example.intro01;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class HelloVerticle extends AbstractVerticle {
    private static final Logger logger = LoggerFactory.getLogger(HelloVerticle.class);

    @Override
    public void start() {
        vertx.createHttpServer().requestHandler(request -> {
            logger.info("request from {}", request.remoteAddress().host());
            request.response().end("Hello");
        }).listen(8080);

        logger.info("server started at localhost:8080");
    }

    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(new HelloVerticle());
    }

}
