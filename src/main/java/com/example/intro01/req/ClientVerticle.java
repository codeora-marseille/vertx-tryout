package com.example.intro01.req;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClientVerticle extends AbstractVerticle {
    public static final Logger logger = LoggerFactory.getLogger(ClientVerticle.class);
    @Override
    public void start() throws Exception {
        vertx.setPeriodic(2000, timerId -> {

            DeliveryOptions deliveryOptions = new DeliveryOptions().addHeader("action", "GET_TIME");
            JsonObject msg = new JsonObject().put("id", "some-information");

            vertx.eventBus().<JsonObject>request("time.server.address", msg, deliveryOptions, asyncResult -> {
                if (asyncResult.succeeded()) {
                    logger.info("get result {}", asyncResult.result().body());
                }
                else {
                    logger.info("request failed {}", asyncResult.cause().getMessage());
                }
            });

        });
    }
}
