package com.example.intro01.req;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonObject;

import java.time.LocalDateTime;

public class TimeServerVerticle extends AbstractVerticle {
    @Override
    public void start() throws Exception {
        vertx.eventBus().<JsonObject>consumer("time.server.address", msg -> {
            String action = msg.headers().get("action");
            switch (action) {
               case "GET_TIME":
                   msg.reply(new JsonObject().put("time", LocalDateTime.now().toString()));
                   break;
                   // TODO: what if we reply with an exception, will it fail or success
               default:
                   msg.fail(-1, "Unknown action " + action);
           }
        });
    }
}
