package com.example.intro01.req;

import io.vertx.core.Vertx;

public class Main {
    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(new TimeServerVerticle());
        vertx.deployVerticle(new ClientVerticle());
    }
}
