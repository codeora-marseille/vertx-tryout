package com.example.intro01.proxy;


import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class InstrumentServiceImpl implements InstrumentService{
    private static final Logger logger = LoggerFactory.getLogger(InstrumentServiceImpl.class);

    private final Map<String, Double> priceMap = new HashMap<>();
    @Override
    public void updateInstrumentService(String instrumentId, Double price, Handler<AsyncResult<Void>> handler) {
        logger.info("updating {} price {} ", instrumentId, price);
        priceMap.put(instrumentId, price);
        handler.handle(Future.succeededFuture());
    }

    @Override
    public void getInstrumentPrice(String instrumentId, Handler<AsyncResult<Double>> handler) {
        logger.info("getting price for {}", instrumentId);
        if (!priceMap.containsKey(instrumentId)) {
            // NOTE: we do not have return values! we return result in an async way
            handler.handle(Future.failedFuture("instrument does not exist: " + instrumentId));
        } else {
            Double price = priceMap.get(instrumentId);
            handler.handle(Future.succeededFuture(price));
        }
    }
}
