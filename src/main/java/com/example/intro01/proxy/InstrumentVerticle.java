package com.example.intro01.proxy;

import io.vertx.core.AbstractVerticle;
import io.vertx.serviceproxy.ServiceBinder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InstrumentVerticle extends AbstractVerticle {
    private static final Logger logger = LoggerFactory.getLogger(InstrumentVerticle.class);
    @Override
    public void start() throws Exception {

        new ServiceBinder(vertx).setAddress("instrument.service")
                .register(InstrumentService.class, new InstrumentServiceImpl());

        logger.info("bind instrument service done");
    }
}
