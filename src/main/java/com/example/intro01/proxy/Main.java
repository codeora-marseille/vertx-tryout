package com.example.intro01.proxy;

import io.vertx.core.Vertx;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(new InstrumentVerticle());

        InstrumentService instrumentService = new InstrumentServiceVertxEBProxy(vertx, "instrument.service");
        instrumentService.updateInstrumentService("abc", 1.23, ar -> {
            if (ar.succeeded()) {
                LOG.info("updated successfully price");

                instrumentService.getInstrumentPrice("abc", par -> {
                    if (par.failed()) {
                        LOG.info("get price failed {}", par.cause().getMessage());
                    } else {
                        LOG.info("get price {}", par.result());
                    }
                });

            }
        });
    }
}
