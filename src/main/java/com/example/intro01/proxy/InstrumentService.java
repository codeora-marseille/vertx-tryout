package com.example.intro01.proxy;

import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;

@ProxyGen
public interface InstrumentService {
    void updateInstrumentService(String instrumentId, Double price, Handler<AsyncResult<Void>> handler);
    void getInstrumentPrice(String instrumentId, Handler<AsyncResult<Double>> handler);
}
