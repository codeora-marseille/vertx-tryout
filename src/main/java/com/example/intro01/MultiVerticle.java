package com.example.intro01;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MultiVerticle extends AbstractVerticle {
    private static final Logger logger = LoggerFactory.getLogger(MultiVerticle.class);

    @Override
    public void start() throws Exception {
        logger.info("{} started working", config().getString("name"));
        // doing some task
    }


    public static void main(String[] args) {
        logger.info("this is main thread");
        String[] names = {"Guy", "Dude", "Buddy"};
        Vertx vertx = Vertx.vertx();
        for (int i = 0; i < names.length; i++) {

            DeploymentOptions deploymentOptions = new DeploymentOptions()
                    .setConfig(new JsonObject().put("name", names[i]))
                    .setInstances(5);

            vertx.deployVerticle(MultiVerticle.class, deploymentOptions);
        }

    }
}
