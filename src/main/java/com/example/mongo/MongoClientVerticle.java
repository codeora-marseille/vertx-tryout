package com.example.mongo;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author zsh
 * 29/Apr/2020
 */
public class MongoClientVerticle extends AbstractVerticle {
  private final Logger logger = LoggerFactory.getLogger(MongoClientVerticle.class);
  // todo, configure with json file
  @Override
  public void start() {
    JsonObject mongoConfig = new JsonObject()
      .put("username", "root")
      .put("password", "root")
      .put("db_name", "vertx_tryout")
      .put("authSource", "admin");
    MongoClient mongo = MongoClient.createShared(vertx, mongoConfig);
    JsonObject user = new JsonObject().put("username", "aturing");
    mongo.insert("users", user, insertAR -> {
      if (insertAR.succeeded()) {
        logger.info("Mongo OK");
      } else {
        logger.info("Mongo KO: " + insertAR.cause().getMessage());
      }
    });
  }

  public static void main(String[] args) {
    Vertx vertx = Vertx.vertx();
    vertx.deployVerticle(new MongoClientVerticle());
  }
}
