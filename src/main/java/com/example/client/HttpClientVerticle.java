package com.example.client;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.client.WebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author zsh
 * 24/Feb/2020
 * this is an HttpClientVerticle that receives requests and calls remote services
 */
public class HttpClientVerticle extends AbstractVerticle {
    private static final Logger LOG = LoggerFactory.getLogger(HttpClientVerticle.class);

    @Override
    public void start(Future<Void> startFuture) {
        Router router = Router.router(vertx);

        WebClient client = WebClient.create(vertx);

        JsonObject conf = config();

        // call localhost:port/hello ==> remote.service.host:port/salute
        // in the unit test we build a mock verticle to reply to this verticle
        router.get("/hello").handler(rc -> {
           client.get(conf.getInteger("remote.service.port"), conf.getString("remote.service.host"), "/salute").send(ar -> {
               if (ar.succeeded()) {
                   LOG.info("REMOTE service response {}", ar.result().bodyAsString());
                   rc.response().end(ar.result().bodyAsString());
               } else {
                   rc.response().setStatusCode(400).end(ar.cause().getMessage());
               }
           });
        });

        vertx.createHttpServer().requestHandler(router).listen(config().getInteger("port"), config().getString("host"), ar ->{
            if (ar.succeeded()) {
                LOG.info("Local server started OK: {}", config());
                startFuture.complete();
            } else {
                LOG.error("Local server start KO");
                startFuture.fail(ar.cause());
            }
        });
    }
}
