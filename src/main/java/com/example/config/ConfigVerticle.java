package com.example.config;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author szhao 2/8/19
 */
public class ConfigVerticle extends AbstractVerticle {
    private static final Logger logger = LoggerFactory.getLogger(ConfigVerticle.class);
    @Override
    public void start() {
        logger.info("n = {}", config().getInteger("n", -1));
    }

    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();
        for (int i = 0; i < 4; i++) {
            JsonObject conf = new JsonObject().put("n", i);
            DeploymentOptions options = new DeploymentOptions().setConfig(conf).setInstances(2);
            vertx.deployVerticle(ConfigVerticle.class.getName(), options);
        }
    }
}
