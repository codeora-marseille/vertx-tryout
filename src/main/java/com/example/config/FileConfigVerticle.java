package com.example.config;

import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author zsh
 * Example illustrating how to read from config json files
 * 21/Feb/2020
 */
public class FileConfigVerticle extends AbstractVerticle {

    private static final Logger LOG = LoggerFactory.getLogger(FileConfigVerticle.class);

    @Override
    public void start(Future<Void> startFuture) {
        LOG.info("FileConfigVerticle started... config {}", config().encode());
        startFuture.complete();
    }

    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();

        ConfigStoreOptions localJsonStore = new ConfigStoreOptions()
                .setType("file")
                .setConfig(new JsonObject().put("path", "config.json"));

        ConfigRetriever retriever = ConfigRetriever.create(vertx,
                new ConfigRetrieverOptions().addStore(localJsonStore));

        retriever.getConfig(ar -> {
            if (ar.failed()) {
                LOG.info("read config failed");
                return;
            }
            DeploymentOptions options = new DeploymentOptions().setConfig(ar.result());

            vertx.deployVerticle(FileConfigVerticle.class.getName(), options, dar -> {
                if (dar.failed()) {
                    LOG.error("Deploy verticle failed");
                    return;
                }
                LOG.info("FileConfigVerticle deployed");
            });
        });



    }
}
