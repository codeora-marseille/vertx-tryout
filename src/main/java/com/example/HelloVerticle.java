package com.example;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

/**
 * @author szhao 2/7/19
 * run server: ./gradlew run -PmainClass=com.example.HelloVerticle
 * client: curl localhost:8088
 */
public class HelloVerticle extends AbstractVerticle {
    private static final Logger logger = LoggerFactory.getLogger(HelloVerticle.class);
    private int counter = 1;

    @Override
    public void start() {
        logger.info("vertx info: {}", vertx);
        vertx.setPeriodic(5000, id -> {
            logger.info("tick " + id);
        });

        vertx.createHttpServer()
                .requestHandler(this::handleRequest)
                .listen(8088, this::handleServerStart);
    }

    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();
        logger.info("main function vertx {}", vertx);

        // deployVerticle sets the vertx instance of the deployed verticle
        vertx.deployVerticle(new HelloVerticle());
    }

    private void handleRequest(HttpServerRequest req) {
        logger.info("request #{} from {}", counter++, req.remoteAddress().host());
        req.response().end("bye!");
    }

    private void handleServerStart(AsyncResult<HttpServer> serverAsyncResult) {
        if (serverAsyncResult.succeeded()) {
            logger.info("starting server: OK");
        }
        else {
            logger.info("starting server: KO {}", serverAsyncResult.cause());
        }
    }
}

