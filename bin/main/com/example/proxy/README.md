To generate service proxy classes, run
```bash
./gradlew build 
```
Then run MainApp, it's normal if your IDE cannot find generated classes and show errors
