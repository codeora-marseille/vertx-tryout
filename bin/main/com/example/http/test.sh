#!/bin/#!/usr/bin/env bash
# run main app and then start this script

welcome=$(curl -X GET \
  'http://localhost:8809/home')

n=0
if [ "$welcome" != "Welcome!" ]; then
  echo "[FAILED] test get home page should return Welcome! But returned $welcome"
  ((n = n + 1))
fi

ticket=$(curl -X GET \
  'http://localhost:8809/tickets/1024')
if [ "$ticket" != "ticket-1024" ]; then
  echo "[FAILED] get ticket should return ticket-1024 but returned $ticket"
  ((n = n + 1))
fi

if [ $n -eq 0 ]; then
  echo "[=== Success ===]"
else
  echo "Total failures: $n"
fi
